# Student Expense Management System
## Backend 
----------------
Python django backend with addons for ease of use

Poetry is used for package management and to handle Virtual Environment.
#### Setup
~~~bash
# Poetry 
python3 -m pip install poetry           # or
pip3 install poetry
~~~
----------------
#### Virtual Environment
~~~bash
# Update or install dependencies
poetry install

# Enter Virtual Environment
poetry shell

# Add Dependency
poetry add <package-name>
~~~
----------------
#### Commands
~~~bash
# Start Server in Development mode
python manage.py runserver <ip>:<port>

# Deploy Server with Daphne
daphne SETS.asgi:application <ip>:<port>

# <ip>:<port> defaults to 127.0.0.1:8000
~~~

----------------

## Fronted
----------------
React js Fronted with additional packages for ease of use

Yarn is used for package managemen.
#### Yarn
~~~bash
# Update or install dependencies
yarn install

# Add Dependency
yarn add <package-name>
~~~
----------------
#### Commands
~~~bash
# Start 
yarn start

# Build
yarn build

# Remove the single build dependency
yarn eject

# Run tests
yarn test
~~~
