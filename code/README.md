# DBMSProject
Dependencies places in pyproject.toml file  
Requires Python3.7 or greater
----------------
## Setup
~~~bash
# Poetry 
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python3 -     #or
python3 -m pip install poetry           # or
pip3 install poetry
~~~
----------------
## Virtual Environment
~~~bash
# Update or install dependencies
poetry install

# Enter Virtual Environment
poetry shell

# Add Dependency
poetry add <package-name>
~~~
