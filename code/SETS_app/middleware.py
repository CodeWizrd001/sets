from django.http.request import HttpHeaders
from SETS_app.models import Usr

import json

class CsrfMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)

        # response['Access-Control-Allow-Credentials'] = True
        response['Access-Control-Expose-Headers'] = '*'
        # print(response['Access-Control-Allow-Headers'])

        return response

class TempMiddleware:

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        try :
            cookies = request.headers['Cookie']
            cookies_ = {}
            for cookie in cookies.split("; ") :
                name , val = cookie.split('=')
                cookies_[name] = val

            headerDict = dict(request.headers)

            headerDict['Cookie'] = 'sessionid='+cookies_['sessionid'] + '; csrftoken=' + cookies_['csrftoken']

            if '127.0.0.1' in headerDict['Host'] :
                headerDict['Host'] = 'localhost:'+headerDict['Host'].split(':')[1]

            tempHeaders = {}

            required = ['Host', 'Cookie', 'Content-Type', 'X-Csrftoken', 'Accept', 'Content-Length']

            for header in headerDict :
                # if header in required :
                tempHeaders['HTTP_' + header] = headerDict[header]

            tempHeaders['HTTP_X-CSRFToken'] = cookies_['csrftoken']

            request.headers = HttpHeaders(tempHeaders)
            # print(request.headers)
        except ValueError:
            pass
        except KeyError :
            pass
        except :
            raise
            pass

        response = self.get_response(request)

        # response['Access-Control-Allow-Credentials'] = True
        response['Access-Control-Expose-Headers'] = '*'
        # print(response['Access-Control-Allow-Headers'])

        return response