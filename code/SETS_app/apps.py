from django.apps import AppConfig


class SetsAppConfig(AppConfig):
    name = 'SETS_app'
