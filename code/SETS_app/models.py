from django.db import models
from django.contrib.auth.models import User
from django.db.models import Model , OneToOneField , CASCADE , AutoField , ForeignKey
from django.db.models import IntegerField , BooleanField , FloatField , CharField
from django.db.models import DateTimeField

# Create your models here.

class Session :
    user = None

class Usr(User) :
    token = CharField(default="None",max_length=256)
    is_loggedin = BooleanField(default=False)

class Admin(models.Model) :
    user = OneToOneField(Usr,on_delete=CASCADE)

    def toDict(self) :
        return {
            "username" : self.user.username ,
            "email" : self.user.email ,
            "first_name" : self.user.first_name ,
            "last_name" : self.user.last_name ,
            "type" : "admin",
        }

class Student(models.Model) :
    user = OneToOneField(Usr,on_delete=CASCADE)
    rollno = CharField(unique=True,default="DXXXXXBR",max_length=10)
    mess = CharField(max_length=32,default=" ")
    dues = FloatField(default=0.0)

    def toDict(self) :
        return {
            "username" : self.user.username ,
            "email" : self.user.email ,
            "rollno" : self.rollno,
            "first_name" : self.user.first_name ,
            "last_name" : self.user.last_name ,
            "dues" : self.dues ,
            "mess" : self.mess ,
            "type" : "student" ,
        }

class Mess(models.Model) :
    user = OneToOneField(Usr,on_delete=CASCADE)
    mess = CharField(max_length=32,unique=True)
    balance = FloatField(default=0.0)
    maxStudents = IntegerField(default=0)
    curStudents = IntegerField(default=0)

    def toDict(self) :
        return {
            "username" : self.user.username ,
            "email" : self.user.email ,
            "first_name" : self.user.first_name ,
            "last_name" : self.user.last_name ,
            "balance" : self.balance ,
            "mess" : self.mess ,
            "max" : self.maxStudents ,
            "cur" : self.curStudents ,
            "type" : "mess",
        }

class Transaction(Model) :
    mess = CharField(max_length=32,default="")
    rollno = CharField(default="",max_length=10)
    amount = FloatField(default=0.0)
    description = CharField(default="",max_length=256)
    date = DateTimeField(auto_now_add=True)

    def toDict(self):
        d , t = str(self.date).split()
        t = t.split('.')[0]
        return {
            'id' : self.id,
            'rollno' : self.rollno ,
            'mess' : self.mess , 
            'amount' : self.amount ,
            'description' : self.description , 
            'date' : d ,
            'time' : t ,
        }