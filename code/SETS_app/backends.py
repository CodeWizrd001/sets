from django.contrib.auth.backends import BaseBackend

from SETS_app.models import User , Admin , Mess , Student , Usr

class UserBackend(BaseBackend):
    def authenticate(self,body) :
        password = body['password']

        try :
            username = body['username']
            user = Usr.objects.get(username=username)
        except KeyError :
            try :
                email =  body['email']
                user = Usr.objects.get(email=email)
            except :
                raise 
        except :
            raise

        if user == None :
            return None 

        if user.check_password(password) :
            return user
        else :
            return None
    
    def get_user(self,user_id) :
        try :
            return Usr.objects.get(id=user_id)
        except :
            return None