ERROR_REQUEST_BODY_MISSING_DATA = "ERROR_REQUEST_BODY_MISSING_DATA"

ERROR_USER_NAME_ALREADY_EXIST = "ERROR_USER_NAME_ALREADY_EXIST"
USER_ALREADY_LOGGED_IN = "USER_ALREADY_LOGGED_IN"
USER_DOES_NOT_EXIST = "USER_DOES_NOT_EXIST"
USER_NOT_LOGGED_IN = "USER_NOT_LOGGED_IN"

ROLLNO_NOT_UNIQUE = "ROLLNO_NOT_UNIQUE"

ERROR_INVALID_EMAIL = "ERROR_INVALID_EMAIL"

INVALID_PERMISSION = "INVALID_PERMISSION"
INVALID_USER = "INVALID_USER"

SUCCESS = "SUCCESS"
FAILED = "FAILED"

class InvalidEmail(BaseException) :
    def __init__(self) :
        pass

class UserDoesNotExist(BaseException) :
    def __init__(self) :
        pass

class InvalidUser(BaseException) :
    def __init__(self) :
        pass

class NotStudent(BaseException) :
    def __init__(self) :
        pass 

class NotAdmin(BaseException) :
    def __init__(self) :
        pass 

class NotMess(BaseException) :
    def __init__(self) :
        pass 