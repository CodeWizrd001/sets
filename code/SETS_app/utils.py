# Functional Imports
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.shortcuts import render
import json
import re

# Model Imports
from SETS_app.models import User , Admin , Mess , Student
from django.contrib.auth.models import AnonymousUser

# Exception Imports
from django.http import Http404
from SETS_app import *

# Model Imports
from SETS_app.models import User , Student , Admin , Mess , Usr

def getUser(request) :
    try :
        token = json.loads(request.body)['token']
        user = Usr.objects.get(token=token)
        if user.is_loggedin :
            return user 
        else :
            return None
    except :
        return None

def GetUser(request) :
    if isinstance(request,Usr) :
        user = request
    else :
        user = getUser(request)

    try :
        return Student.objects.get(user=user)
    except :
        pass 
    try :
        return Mess.objects.get(user=user)
    except :
        pass
    try :
        return Admin.objects.get(user=user)
    except:
        pass 

    return None 

def is_loggedin(function) :
    def check(*args,**kwargs) :
        request = args[0]
        if not GetUser(request) :
            return Response({"STATUS" : USER_NOT_LOGGED_IN })

        return function(*args,**kwargs)

    return check

def is_admin(function) :
    def check(*args,**kwargs) :
        request = args[0]
        if not GetUser(request) :
            return Response({"STATUS" : USER_NOT_LOGGED_IN })
        if not isinstance(GetUser(request),Admin) :
            return Response({"STATUS" : NOT_ADMIN})
        return function(*args,**kwargs)

    return check

def isEmailValid(email) :
    try :
        assert re.match('[a-z,0-9,_]+@nitc.ac.in',email) , "Invalid Mail"
    except AssertionError:
        raise InvalidEmail
    except :
        raise

def isTypeValid(userType) :
    if userType in ['student','mess','admin'] :
        return
    raise Http404("Invalid Path")