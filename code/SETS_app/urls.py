from django.urls import path,include
from SETS_app.views import common , admin , student , mess

urlpatterns = [
    path('<str:userType>/register', common.Register),
    path('<str:userType>/login',common.Login),
    path('<str:userType>/logout',common.Logout),
    path('<str:userType>/get',common.getUser),
    # Admin Paths
    path('admin/getusers/<str:typ>',admin.get_users),
    path('admin/getusers',admin.get_users),
    path('admin/adduser',admin.add_user),
    path('admin/deluser',admin.remove_user),
    # Mess Paths
    path('mess/getusers',mess.get_users),
    path('mess/transactions',mess.view_transactions) ,
    path('mess/addtransaction',mess.add_transaction) ,
    # Student Paths 
    path('student/dues',student.view_dues),
    path('student/transactions',student.view_transactions),
    path('student/changemess',student.change_mess),
    path('student/getmess',student.get_mess),
]