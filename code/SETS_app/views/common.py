# Functional Imports
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.shortcuts import render

# Utility Imports
from passlib.hash import pbkdf2_sha256
import datetime
import random
import json
import time

# Custom Error Imports
from django.db.utils import DatabaseError
from SETS_app import *

# Authentication Imports
from django.contrib.auth.decorators import login_required , permission_required
from django.contrib.auth import login , logout , authenticate

# Exception Imports
from django.http.request import RawPostDataException
from django.db.utils import IntegrityError 

# Model Imports
from SETS_app.models import User , Student , Admin , Mess , Usr

# Utility Imports
from SETS_app.utils import isEmailValid , isTypeValid , GetUser , is_loggedin , getUser


@api_view(["POST"])
def Register(request,userType):
    isTypeValid(userType)

    body = json.loads(request.body)

    try:
        isEmailValid(body['email'])
        user = Usr(
                username=body['username'] ,
                email=body['email'] ,
                first_name=body['first_name'] ,
                last_name=body['last_name']
            )
        user.set_password(body['password'])
        user.save()
        if userType == "student" :
            student = Student(user=user,rollno=data['rollno'],mess=data['mess'])
            student.save()
        elif userType == "mess" :
            mess = Mess(user=user,mess=data['mess'],maxStudents=data['max'])
            mess.save()
        elif userType == "admin" :
            admin = Admin(user=user)
            admin.save()
        return Response({"STATUS": SUCCESS})
    except InvalidEmail :
        return Response({'STATUS' : ERROR_INVALID_EMAIL})
    except KeyError:
        try :
            user.delete()
        except :
            pass 
        return Response({"STATUS": ERROR_REQUEST_BODY_MISSING_DATA})
    except DatabaseError:
        try :
            user.delete()
        except :
            pass 
        return Response({"STATUS": ERROR_USER_NAME_ALREADY_EXIST})
    except:
        raise

@api_view(["POST"])
def Login(request,userType):
    isTypeValid(userType)

    try:
        body = json.loads(request.body)
        user = authenticate(body)
        userData = GetUser(user)

        if GetUser(request) :
            raise RawPostDataException
        try :
            if userType == 'student' :
                assert isinstance(userData,Student)
            elif userType == 'mess' :
                assert isinstance(userData,Mess)
            elif userType == 'admin' :
                assert isinstance(userData,Admin)
        except AssertionError :
            return Response({"STATUS": FAILED})
        if user is not None:
            # login(request, user)
            data = dict(userData.toDict())
            digs = 1000000000
            data['token'] = int(random.random() * digs)  # // 1
            data['token'] = str(data['token'])
            user.token = data['token']
            user.is_loggedin = True
            user.save()
            data['STATUS'] = SUCCESS
            return Response(data)
        else:
            return Response({"STATUS": FAILED})
    except RawPostDataException:
        return Response({'STATUS' : USER_ALREADY_LOGGED_IN})
    except Usr.DoesNotExist :
        return Response({'STATUS' : USER_DOES_NOT_EXIST})
    except KeyError :
        return Response({"STATUS": ERROR_REQUEST_BODY_MISSING_DATA})

@api_view(["POST"])
@is_loggedin
def getUser(request,userType) :
    isTypeValid(userType)

    try :
        if userType == 'student' :
            assert isinstance(GetUser(request),Student)
        elif userType == 'mess' :
            assert isinstance(GetUser(request),Mess)
        elif userType == 'admin' :
            assert isinstance(GetUser(request),Admin)
    except AssertionError :
        return Response({"STATUS" : INVALID_USER})

    return Response(GetUser(request).toDict())

@api_view(["POST"])
@is_loggedin
def Logout(request,userType):
    isTypeValid(userType)

    try :
        if userType == 'student' :
            assert isinstance(GetUser(request),Student)
        elif userType == 'mess' :
            assert isinstance(GetUser(request),Mess)
        elif userType == 'admin' :
            assert isinstance(GetUser(request),Admin)
    except AssertionError :
        return Response({"STATUS" : INVALID_USER})

    user = getUser(request)
    user.token = ""
    user.is_loggedin = False
    user.save()

    return Response({"STATUS": SUCCESS})
