# Functional Imports
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.shortcuts import render

# Utility Imports
from passlib.hash import pbkdf2_sha256
import datetime
import json
import time

# Custom Error Imports
from django.db.utils import DatabaseError
from SETS_app import *

# Authentication Imports
from django.contrib.auth.decorators import login_required , permission_required
from django.contrib.auth import login , logout , authenticate

# Exception Imports
from django.http.request import RawPostDataException
from django.db.utils import IntegrityError 

# Model Imports
from SETS_app.models import User , Student , Admin , Mess , Transaction

# Utility Imports
from SETS_app.utils import isEmailValid , isTypeValid , GetUser , is_loggedin

@api_view(["POST"])
@is_loggedin
def view_dues(request) :
    user = GetUser(request)

    try :
        assert isinstance(user,Student)
    except AssertionError :
        return Response({"STATUS" : INVALID_USER})
    return Response({
        "STATUS" : SUCCESS ,
        "Due" : user.dues,
        })

@api_view(["POST"])
@is_loggedin
def view_transactions(request) :
    user = GetUser(request)

    try :
        assert isinstance(user,Student)
    except AssertionError :
        return Response({"STATUS" : INVALID_USER})

    transactions = Transaction.objects.filter(rollno=user.rollno)

    return Response({
        "STATUS" : SUCCESS , 
        "transactions" : [i.toDict() for i in transactions]
    })

@api_view(["POST"])
@is_loggedin
def change_mess(request) :
    user = GetUser(request)

    try :
        assert isinstance(user,Student)
    except AssertionError :
        return Response({"STATUS" : INVALID_USER})

    try :
        data = request.data
        oldMess = Mess.objects.get(mess=user.mess)
        newMess = Mess.objects.get(mess=data['newMess'])

        if newMess.curStudents < newMess.maxStudents :
            newMess.curStudents += 1
            oldMess.curStudents -= 1
            user.mess = newMess.mess
            user.save()
            newMess.save()
            oldMess.save()
            return Response({
                "STATUS" : SUCCESS ,
            })
        else :
            return Response({
                "STATUS" : FAILED ,
                "Reason" : "MESS_FULL",
            })
    except KeyError :
        return Response({"STATUS" : ERROR_REQUEST_BODY_MISSING_DATA})

@api_view(["POST"])
@is_loggedin
def get_mess(request) :
    data = {}

    try :
        mess = Mess.objects.all()
    except :
        mess = []
    
    data['Mess'] = [i.toDict()['mess'] for i in mess]
    data['Mess'] = [{'mess' : i} for i in sorted(list(set(data['Mess'])))]
    
    data['STATUS'] = SUCCESS

    return Response(data)