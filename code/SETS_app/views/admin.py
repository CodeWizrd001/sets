# Functional Imports
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.shortcuts import render
from rest_framework import status

# Utility Imports
from passlib.hash import pbkdf2_sha256
import datetime
import json
import time

# Custom Error Imports
from django.db.utils import DatabaseError
from SETS_app import *

# Authentication Imports
from django.contrib.auth.decorators import login_required , permission_required
from django.contrib.auth import login , logout , authenticate

# Exception Imports
from django.http.request import RawPostDataException
from django.db.utils import IntegrityError 

# Model Imports
from SETS_app.models import User , Student , Admin , Mess , Usr , Transaction

# Utility Imports
from SETS_app.utils import isEmailValid , isTypeValid , GetUser , is_loggedin , is_admin

@api_view(["POST"])
@is_admin
def add_user(request) :
    user = GetUser(request)
    data = request.data

    try:
        userType = data['type']
        isEmailValid(data['email'])

        user = Usr(
                username=data['username'] ,
                email=data['email'] ,
                first_name=data['first_name'] ,
                last_name=data['last_name']
            )
        user.set_password(data['password'])
        user.save()
        if userType == "s" :
            student = Student(user=user,rollno=data['rollno'],mess=data['mess'])
            student.save()
        elif userType == "m" :
            mess = Mess(user=user,mess=data['mess'],maxStudents=data['max'])
            mess.save()
        else :
            user.delete()
            return Response({"STATUS" : FAILED})
        return Response({"STATUS": SUCCESS})
    except InvalidEmail :
        return Response({'STATUS' : ERROR_INVALID_EMAIL})
    except KeyError:
        try :
            user.delete()
        except :
            pass 
        return Response({"STATUS" : ERROR_REQUEST_BODY_MISSING_DATA})
    except DatabaseError:
        try :
            user.delete()
        except :
            pass 
        return Response({"STATUS": ERROR_USER_NAME_ALREADY_EXIST})
    except:
        raise

@api_view(['POST'])
@is_admin
def remove_user(request) :
    data = {}

    body = request.data

    try :
        userType = body['type']
        username = body['username']
        user = Usr.objects.get(username=username)
        if userType == 's' :
            user_ = Student.objects.get(user=user)
        elif userType == 'm' :
            user_ = Mess.objects.get(user=user)
        else :
            raise Exception("Invalid UserType")
        user_.delete()
        user.delete()
        data['STATUS'] = SUCCESS
    except KeyError :
        data['STATUS'] = ERROR_REQUEST_BODY_MISSING_DATA
    except :
        data['STATUS'] = FAILED

    return Response(data)

@api_view(["POST"])
@is_admin
def get_users(request,typ="all") :
    data = {}
    if typ not in ['student','mess','all'] :
        return Response({},status=status.HTTP_404_NOT_FOUND)
    if typ in ["student",'all'] :
        try :
            students = Student.objects.all()
        except Student.DoesNotExist :
            students = []
            pass
        data['Students'] = [i.toDict() for i in students]
    if typ in ['mess','all'] :
        try :
            mess = Mess.objects.all()
        except Mess.DoesNotExist:
            mess = []
            pass
        data['Mess'] = [i.toDict() for i in mess]
    
    data['STATUS'] = SUCCESS

    return Response(data)

@api_view(['POST'])
@is_admin
def get_transactions(request) :
    return Response({})