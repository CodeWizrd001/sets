# Functional Imports
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.shortcuts import render

# Utility Imports
from passlib.hash import pbkdf2_sha256
import datetime
import json
import time

# Custom Error Imports
from django.db.utils import DatabaseError
from SETS_app import *

# Authentication Imports
from django.contrib.auth.decorators import login_required , permission_required
from django.contrib.auth import login , logout , authenticate

# Exception Imports
from django.http.request import RawPostDataException
from django.db.utils import IntegrityError 

# Model Imports
from SETS_app.models import User , Student , Admin , Mess , Transaction

# Utility Imports
from SETS_app.utils import isEmailValid , isTypeValid , GetUser , is_loggedin

@api_view(["POST"])
@is_loggedin
def get_users(request) :
    data = {}

    user = GetUser(request)

    try :
        assert isinstance(user,Mess)
    except AssertionError :
        return Response({"STATUS" : INVALID_USER})

    try :
        mess = Student.objects.filter(mess=user.mess)
    except Mess.DoesNotExist:
        mess = []
        pass
    data['Students'] = [i.toDict() for i in mess]
    
    data['STATUS'] = SUCCESS

    return Response(data)

@api_view(["POST"])
@is_loggedin
def view_transactions(request) :
    user = GetUser(request)

    try :
        assert isinstance(user,Mess)
    except AssertionError :
        return Response({"STATUS" : INVALID_USER})

    transactions = Transaction.objects.filter(mess=user.mess)

    return Response({
        "STATUS" : SUCCESS , 
        "transactions" : [i.toDict() for i in transactions]
    })

@api_view(["POST"])
@is_loggedin
def add_transaction(request) :
    user = GetUser(request)

    try :
        assert isinstance(user,Mess)
    except AssertionError :
        return Response({"STATUS" : INVALID_USER})

    try :
        data = request.data

        t = Transaction(
            mess=user.mess , 
            rollno=data['rollno'] ,
            amount=float(data['amount']) ,
            description=data['description'] ,
        )

        student = Student.objects.get(rollno=data['rollno'])
        student.dues += t.amount

        student.save()
        t.save()

        return Response({"STATUS" : SUCCESS})
    except KeyError :
        return Response({"STATUS" : ERROR_REQUEST_BODY_MISSING_DATA})
    except :
        return Response({"STATUS" : FAILED})