import React , {useRef , useEffect , useState } from 'react'
import {logout} from '../scripts/common'
import { Form , Button , Card , Alert } from 'react-bootstrap'

import makeRequest from '../scripts/common'

export default function Student(httpObject) {
    async function logoutButton(){
        logout(httpObject)
        window.location.href = "/"
    }

    const [dues,setDues] = useState(0.0)

    async function getDues(){
        const duesData = await makeRequest("/student/dues",{})
        setDues(duesData['data']['Due'])
    }
    getDues()

    return(
        <div>
            <h1>Student</h1>
            <h4>DUE : {dues}</h4>

            <ul>
                <li><a href="/student/transactions">View Transactions</a></li>
                <li><a href="/student/changemess">Change Mess</a></li>
                <li><a onClick={logoutButton}> Logout</a></li>
            </ul>
        </div>
    );
}
