import React , { useRef, useState } from 'react'
import { Form , Button , Card , Alert } from 'react-bootstrap'
import makeRequest , { cookies , logout }from '../scripts/common'

export default function ChangeMess(httpObject) {
    async function logoutButton(){
        logout(httpObject)
        window.location.href = "/"
    }

    const newMessRef = useRef()

    const [options,setOptions] = useState([])
    const [mess,setMess] = useState('A')
    

    async function handleSubmit(e) {
        e.preventDefault()
        const data = await makeRequest("/student/changemess",{'newMess' : mess})
        console.log(data)
        if(data['data']['STATUS'] == 'USER_NOT_LOGGED_IN'){
            window.location.href = "/student/login"
        }
        else if(data['data']['STATUS'] == 'SUCCESS'){
            alert("Mess Changed")
        }
        else {
            alert("Failed\nMess Full Or Unknown Error")
        }
    }

    async function loadOptions(){
        const optionsData = await makeRequest("/student/getmess",{})
        if(optionsData['data']['STATUS'] == 'USER_NOT_LOGGED_IN'){
            window.location.href = "/student/login"
        }
        setOptions(optionsData['data']['Mess']
        .map((m) => 
            <option>{m.mess}</option>))
        setMess(optionsData['data']['Mess'][0]['mess'])
    }
    loadOptions()

    function handleChange(e) {
        console.log(e.target.value)
        setMess(e.target.value)
    }

    return(
        <div>
            <Card>
                <Card.Body>
                <h2 className="text-center mb-4">Change Mess</h2>
                <Form onSubmit={handleSubmit}>
                    <Form.Group id="email">
                        <h5>New Mess</h5>
                        <Form.Control as="select" onChange={handleChange} required >
                            { options }
                        </Form.Control>
                    </Form.Group>
                    <Button className="w-100" type="submit">
                        Change
                    </Button>
                </Form>
                </Card.Body>
            </Card>
        </div>
    );
}