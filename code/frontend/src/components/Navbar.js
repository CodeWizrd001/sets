import React from 'react'
import { Navbar,Nav } from 'react-bootstrap';

const Navheader = ()=>{
    return (
        <div>
            <div className="row">
                <div className="col-md-12">
                    <Navbar bg="light" expand ="lg" sticky="top">
                        <Navbar.Brand href ="#">SETS</Navbar.Brand>
                            <Navbar.Toggle aria-controls="basic-navbar-nav" />
                            <Navbar.Collapse id="basic-navbar-nav">
                                <Nav className="mr-auto">
                                    <Nav.Link href="/">Home</Nav.Link>
                                    <Nav.Link href="/student/login">Login</Nav.Link>
                                    <Nav.Link href="/">Contact Developer</Nav.Link>
                                </Nav>
                            </Navbar.Collapse>
                    </Navbar>
                </div>
            </div>
        </div>
    )
}

export default Navheader