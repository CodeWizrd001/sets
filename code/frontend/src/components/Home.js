import React from 'react'
import { Form , Button , Card , Alert } from 'react-bootstrap'

const Home=()=>{
    return (
        <div>
            <ul>
                <li><a href="/admin/login">Admin</a></li>
                <li><a href="/student/login">Student</a></li>
                <li><a href="/mess/login">Mess</a></li>
            </ul>
        </div>
    );
}

export default Home