import React , {useRef} from 'react'
import { Form , Button , Card , Alert } from 'react-bootstrap'
import {Link} from 'react-router-dom';
import makeRequest from '../scripts/common';

function Register(httpObject) {
  const messRef = useRef()
  const emailRef = useRef()
  const unameRef = useRef()
  const fnameRef = useRef()
  const lnameRef = useRef()
  const rollNoRef = useRef()
  const maxStudRef = useRef()
  const passwordRef = useRef()
  const passwordConfirmRef = useRef()

  // Get UserType
  const userType = httpObject.location.pathname.split('/')[1]
  console.log(userType)

  async function handleSubmit(e) {
        e.preventDefault()

        if(passwordRef.current.value !== passwordConfirmRef.current.value) {
            console.log("pwd doesnt match")
            return
        }

        const requestBody = {
          'username':unameRef.current.value ,
          'email':emailRef.current.value,
          'first_name':fnameRef.current.value,
          'last_name':lnameRef.current.value,
          'password':passwordRef.current.value,
        }

        try {
          requestBody['rollno'] = rollNoRef.current.value
        }
        catch(error){
          console.log("Error",error)
        }

        try {
          requestBody['mess'] = messRef.current.value
        }
        catch(error){
          console.log("Error",error)
        }

        try {
          requestBody['max'] = maxStudRef.current.value
        }
        catch(error){
          console.log("Error",error)
        }

        const data = await makeRequest('/'+userType+'/register',requestBody)

        console.log(data)
        if(data['data']['STATUS'] == 'SUCCESS'){
          window.location.href = '/'+userType+'/login'
        }
    }
    return (
       <>
        <Card>
        <Card.Body>
        <h2 className="text-center mb-4">Sign Up</h2>

          <Form onSubmit={handleSubmit}>
            <Form.Group id="username">
              <h5>Username</h5>
              <Form.Control type="text" ref={unameRef} required />
            </Form.Group>
            <Form.Group id="firstname">
              <h5>First Name</h5>
              <Form.Control type="text" ref={fnameRef} required />
            </Form.Group>
            <Form.Group id="lastname">
              <h5>Last Name</h5>
              <Form.Control type="text" ref={lnameRef} required />
            </Form.Group>
            {
              userType == 'student' ?
              <Form.Group id="rollno">
                <h5>Roll Number</h5>
                <Form.Control type="text" ref={rollNoRef}  />
              </Form.Group>
              : null
            }
            {   userType == 'mess' ?
                <Form.Group id="maxstud">
                <h5>Maximum Students</h5>
                <Form.Control type="text" ref={maxStudRef}  />
                </Form.Group>
                : null
            }
            {
              userType == 'mess' || userType == 'student' ?
              <Form.Group id="mess">
                <h5>Mess</h5>
                <Form.Control type="text" ref={messRef}  />
              </Form.Group>
              : null
            }
            <Form.Group id="email">
              <h5>Email</h5>
              <Form.Control type="email" ref={emailRef} required />
            </Form.Group>
            <Form.Group id="password">
              <h5>Password</h5>
              <Form.Control type="password" ref={passwordRef} required />
              <Form.Text id="passwordHelpBlock" muted>
                </Form.Text>
            </Form.Group>
            <Form.Group id="password-confirm">
              <h5>Confirm Password</h5>
              <Form.Control type="password" ref={passwordConfirmRef} required />
            </Form.Group>
            <Button className="w-100" type="submit">
              Sign Up
            </Button>
          </Form>
        </Card.Body>
      </Card>
      <div>
        Already have an account? <Link to="/login">Log In</Link>
      </div>
    </>
   
    )
}

export default Register