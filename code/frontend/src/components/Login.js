import React , { useRef, useState } from 'react'
import { Form , Button , Card , Alert } from 'react-bootstrap'
import {Link , useHistory } from 'react-router-dom'
import makeRequest , { cookies , logout }from '../scripts/common'

function Login(httpObject) {
  const emailRef = useRef();
  const passwordRef = useRef();
  const [error , setError] = useState("");
  const [loading, setLoading] = useState(false);
  const history  = useHistory();
  // Get UserType
  const userType = httpObject.location.pathname.split('/')[1]
  console.log(userType)

  logout(userType)

  async function handleSubmit(e) {
        e.preventDefault()

        // Make Request
        const data = await makeRequest("/"+userType+"/login",{
          'email':emailRef.current.value ,
          'password':passwordRef.current.value,
        })
        console.log("Login : ",data)

        // Login
        if(data['data']['token']){
          localStorage.setItem('token',data['data']['token'])
          cookies.set('token',data['data']['token'])
          console.log("Redirecting")
          window.location.href = '/'+userType+'/dashboard'
        }

        // Already Logged In
        if(data['data']['STATUS'] == 'USER_ALREADY_LOGGED_IN'){
          console.log("Redirecting")
          window.location.href = '/'+userType+'/dashboard'
        }

        if(data['data']['STATUS'] == 'FAILED'){
          logout(userType)
        }

        alert("Login Failed")
    }

    return (
       <>
        <Card>
        <Card.Body>
          <h2 className="text-center mb-4">Login In</h2>
          
          {error && <Alert variant="danger">{error}</Alert>}
          <Form onSubmit={handleSubmit}>
            <Form.Group id="email">
              <h5>Email</h5>
              <Form.Control type="email" ref={emailRef} required />
            </Form.Group>
            <Form.Group id="password">
             <h5>Password</h5>
              <Form.Control type="password" ref={passwordRef} required />     
            </Form.Group>
            <Button disabled={loading} className="w-100" type="submit">
              Login In
            </Button>
          </Form>
        </Card.Body>
      </Card>
      <div className="w-100 text-center mt-2">
        Don't have an account? <Link to={"/"+userType+"/register"}>Sign Up</Link>
      </div>
    </>
   
    )
}

export default Login 