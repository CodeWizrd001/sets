import React from 'react';
import {Nav} from 'react-bootstrap';

const Dashboard=()=>{
    return (
        <ul>
            <li><a href="/">Home</a></li>
            <li><a href="/student/login">Login</a></li>
            <li><a href="/student/register">Register</a></li>
        </ul>
    );
}

export default Dashboard