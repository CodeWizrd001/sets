import Cookies from 'universal-cookie';

const url = "127.0.0.1"
const http = "http://"+url+":8000"
const ws = "ws://"+url+":8000"

const cookies = new Cookies();

function getCookie(name) {
    console.log("Cookies Got : ",cookies.get(name))
    console.log(document.cookie)
    return cookies.get(name)
}

function getToken(){
    // console.log("GetToken")
    try {
        const tok = localStorage.getItem('token') // cookies.get('token') // 
        // console.log("Token ",tok)
        return tok
    }
    catch (e){
    return null
    }
}

async function makeRequest(endpoint,body) {
    const requestOptions = {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: body
    };

    const userType = endpoint.split('/')[1]

    if(getToken()){
        requestOptions.body['token'] = getToken()
    }

    requestOptions.body = JSON.stringify(requestOptions.body)

    //console.log(requestOptions)

    const response = await fetch(http+endpoint, requestOptions)
    const data = await response.json()
    const headers = response.headers

    //console.log("After Request",data['csrftoken'])
    //console.log(response)

    //for(const header of headers){
    //    console.log(header.toString());
    //}

    return {
        'headers' : headers,
        'data' : data ,
    };
}

async function logout(httpObject) {
    try{
    // Get UserType
    localStorage.removeItem('token')

    const userType = httpObject.location.pathname.split('/')[1]
    const data = makeRequest('/'+userType+'/logout',{})

    console.log(userType+" Logged Out")
    }
    catch(e){
        console.log("Error ",e)
    }
}

export default makeRequest
export { cookies , getCookie , logout }