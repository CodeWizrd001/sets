import React from "react"
import "./App.css"

import Home from "./components/Home"

import Register from "./components/Register"

import Admin from "./admin/Admin"
import Addusers from "./admin/Addusers"
import Users from "./admin/users"

import Student from "./student/Student"
import ChangeMess from "./student/Change"
import sTransactions from "./student/Transactions"

import Mess from './mess/Mess'
import mAdd from './mess/Add'
import mUsers from './mess/Users'
import mTransactions from './mess/Transactions'

import { Container } from "react-bootstrap";
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import Dashboard from "./components/Dashboard";
import Login from './components/Login'

const Page404 = ({ location }) => {
  return (
    <div>
      <h2>No match found for <code>{location.pathname}</code></h2>
    </div>
  )
};

function App() {
  return (
    <div>
      <Container
        className="d-flex align-items-center justify-content-center"
        style={{ minHeight: "100vh" }}
      >
        <div className="w-100" style={{ maxWidth: "400px" }}>
          <Router>
            <Switch>

                <Route exact path="/">
                  <Redirect to="/home" />
                </Route>
                <Route path="/home" component={Home} />
                <Route path="/dashboard" component={Dashboard} />
                <Route path="/student/register" component={Register} />
                <Route path="/student/login" component={Login} />
                <Route path="/student/dashboard" component={Student} />
                <Route path="/student/changemess" component={ChangeMess} />
                <Route path="/student/transactions" component={sTransactions} />

                <Route path="/admin/register" component={Register} />
                <Route path="/admin/login" component={Login} />
                <Route path="/admin/dashboard" component={Admin} />
                <Route path="/admin/users" component={Users} />
                <Route path="/admin/addusers" component={Addusers} />

                <Route path="/mess/login" component={Login} />
                <Route path="/mess/users" component={mUsers} />
                <Route path="/mess/dashboard" component={Mess} />
                <Route path="/mess/register" component={Register} />
                <Route path='/mess/addtransactions' component={mAdd} />
                <Route path='/mess/transactions' component={mTransactions} />
                <Route component={Page404}/>
              </Switch>
          </Router>
        </div>
      </Container>
    </div>
  );
}

export default App;