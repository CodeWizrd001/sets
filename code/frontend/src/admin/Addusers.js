import React , {useRef , useEffect , useState } from 'react'
import { Form , Button , ButtonGroup , Card , Alert } from 'react-bootstrap'
import {Link} from 'react-router-dom';
import makeRequest from '../scripts/common';

const Addusers = ()=>{  
    const messRef = useRef()
    const emailRef = useRef()
    const unameRef = useRef()
    const fnameRef = useRef()
    const lnameRef = useRef()
    const rollNoRef = useRef()
    const maxStudRef = useRef()
    const passwordRef = useRef()
    const [student,setStudent] = useState(false)
    const [userType,setType] = useState("m")
    const [options,setOptions] = useState([])

    var mess = "" 

    async function handleSubmit(e) {
        e.preventDefault()

        const requestBody = {
            'username':unameRef.current.value ,
            'email':emailRef.current.value,
            'first_name':fnameRef.current.value,
            'last_name':lnameRef.current.value,
            'password':passwordRef.current.value,
            'type':userType
        }

        try {
            requestBody['rollno'] = rollNoRef.current.value;
        }
        catch(error){
            console.log("Error",error)
        }

        try {
            requestBody['mess'] = messRef.current.value
        }
        catch(error){
            requestBody['mess'] = mess
            console.log("Error",error)
        }

        try {
            requestBody['max'] = maxStudRef.current.value
        }
        catch(error){
            console.log("Error",error)
        }

        console.log(requestBody)

        const data = await makeRequest('/admin/adduser',requestBody)

        console.log(data)
        if(data['data']['STATUS'] == "SUCCESS"){
            alert("User Added")
        }
        else if(data['data']['STATUS'] == 'USER_NOT_LOGGED_IN'){
            alert("You're Not Logged In")
            window.location.href="/admin/login"
        }
        else {
            alert("Failed To Add User\nCheck Data Entered")
        }
    }

    async function getMess() {
        const data = await makeRequest('/admin/getusers/mess',{})
        setOptions(data['data']['Mess']
            .map((m)=>
                <option>{m.mess}</option>
            ))
        mess = options[0]
        console.log(options)
    }

    function handleMessChange(e) {
        mess = e.target.value
    }

    function handleChange(e){
        e.preventDefault()
        getMess()
        const type = e.target.value
        if(type == "Student"){
            setStudent(true)
            setType('s')
        }
        else if(type == "Mess"){
            setStudent(false)
            setType('m')
        }
        else {
            console.log("Unselected")
        }
    }

    return(
        <div>
            <Card>
                <Card.Body>
                <h2 className="text-center mb-4">Add { student? "Student" : "Mess"}</h2>

                <Form onSubmit={handleSubmit}>
                    <Form.Group id="username">
                    <h5>Username</h5>
                    <Form.Control type="text" ref={unameRef} required />
                    </Form.Group>
                    <Form.Group id="firstname">
                    <h5>First Name</h5>
                    <Form.Control type="text" ref={fnameRef} required />
                    </Form.Group>
                    <Form.Group id="lastname">
                    <h5>Last Name</h5>
                    <Form.Control type="text" ref={lnameRef} required />
                    </Form.Group>
                    <Form.Group controlId="userType">
                        <h5>User Type</h5>
                        <Form.Control as="select" onChange={handleChange} required >
                            <option>Mess</option>
                            <option>Student</option>
                        </Form.Control>
                    </Form.Group>
                    {   student ?
                        <Form.Group id="rollno">
                        <h5>Roll Number</h5>
                        <Form.Control type="text" ref={rollNoRef}  />
                        </Form.Group>
                        : null
                    }
                    {   !student ?
                        <Form.Group id="maxstud">
                        <h5>Maximum Students</h5>
                        <Form.Control type="text" ref={maxStudRef}  />
                        </Form.Group>
                        : null
                    }
                    <Form.Group id="mess">
                    <h5>Mess</h5>
                    {
                        student ?
                        <Form.Control name="mess" as="select" onChange={handleMessChange} required >
                            { options }
                        </Form.Control>
                        :
                        <Form.Control type="text" ref={messRef}  />
                    }
                    </Form.Group>
                    <Form.Group id="email">
                    <h5>Email</h5>
                    <Form.Control type="email" ref={emailRef} required />
                    </Form.Group>
                    <Form.Group id="password">
                    <h5>Password</h5>
                    <Form.Control type="password" ref={passwordRef} required />
                    <Form.Text id="passwordHelpBlock" muted>
                        </Form.Text>
                    </Form.Group>
                    <Button className="w-100" type="submit">
                        Add { student? "Student" : "Mess"}
                    </Button>
                </Form>
                </Card.Body>
            </Card>
        </div>
    );
};

export default Addusers