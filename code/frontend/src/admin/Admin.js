import React from 'react';
import {logout} from '../scripts/common'
import { Form , Button , Card , Alert } from 'react-bootstrap'

const Admin = (httpObject)=>{
    async function logoutButton(){
        logout(httpObject)
        window.location.href = "/"
    }

    return(
        <div>
        <ul>
            <li><a href="/admin/addusers">Add Users</a></li>
            <li><a href="/admin/users">View Users</a></li>
            <li><a href="/home">Log out</a></li>
        </ul>
        </div>
    );
};

export default Admin