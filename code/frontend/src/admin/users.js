import React, {Component} from 'react'
import { Tabs , Tab, Table, Button } from 'react-bootstrap'
import axios from '../axios'
import makeRequest from '../scripts/common'

export default class Users extends Component {
    constructor(props) {
        super(props);
        this.i = 0
        this.state = {
            Mess: [],
            Students: [],
        };
    }

    async deleteUser(event){
        const username = event.target.attributes.username.value
        const type = event.target.attributes.type.value
        if(window.confirm("Are You Sure You Want To Delete "+username)){
            const data = makeRequest("/admin/deluser",{'username':username,"type":type})
            console.log("Deleted User")
        }
        else{
            console.log("Delete Cancelled")
        }
        this.getUsersData()
    }

    async getUsersData() {
        const data = await makeRequest('/admin/getusers',{})
        console.log(data)
        if(data['data']['STATUS'] == 'USER_NOT_LOGGED_IN'){
            alert("You're Not Logged In")
            window.location.href="/admin/login"
        }
        try {
            const allm = data['data']['Mess']
            .map((m) => 
                <tr>
                    <td>{m.first_name}</td>
                    <td>{m.last_name}</td>
                    <td>{m.mess}</td>
                    <td onClick={this.deleteUser.bind(this)} username={m.username} type='m'> X </td>
                </tr>)
            this.setState({'Mess':allm.slice(this.i*8,(this.i+1)*8)})
            const alls =data['data']['Students']
            .map((s) => 
                <tr>
                    <td>{s.first_name}</td>
                    <td>{s.last_name}</td>
                    <td>{s.rollno}</td>
                    <td>{s.mess}</td>
                    <td onClick={this.deleteUser.bind(this)} username={s.username} type='s'> X </td>
                </tr>)
            this.setState({'Students':alls.slice(this.i*8,(this.i+1)*8)})
        }
        catch(error){
            console.log("GetUserError",error)
        }
    }

    prev(){
        this.i -= 1
        if(this.i < 0)
            this.i = 0
        this.getUsersData()
    }

    next(){
        this.i += 1
        this.getUsersData()
    }

    modeChange() {
        this.i = 0
        this.getUsersData()
    }

    componentDidMount(){
        this.getUsersData()
    }
    render() {
        return (
            <div id="anchored">
                <Tabs className="tabletab" defaultActiveKey='Student' variant="pills" onSelect={this.modeChange.bind(this)}>
                    <Tab className="tabs" eventKey='Student' title='Student'>
                    <Table striped bordered hover>
                            <thead>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Roll Number</th>
                                <th>Mess</th>
                                <th>Delete</th>
                            </thead>
                            <tbody>
                                {this.state.Students}
                            </tbody>
                        </Table>
                    </Tab>
                    <Tab className="tabs" eventKey='Mess' title='Mess'>
                        <Table striped bordered hover>
                            <thead>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Mess</th>
                                <th>Delete</th>
                            </thead>
                            <tbody>
                                {this.state.Mess}
                            </tbody>
                        </Table>
                    </Tab>
                </Tabs>
                <Table striped bordered hover>
                    <thead>
                        <th>
                            <Button onClick={this.prev.bind(this)}>
                                Prev
                            </Button>
                        </th>
                        <th>
                            Page {this.i + 1}
                        </th>
                        <th>
                            <Button onClick={this.next.bind(this)}>
                                Next
                            </Button>
                        </th>
                    </thead>
                </Table>
            </div>
        )
    }
}