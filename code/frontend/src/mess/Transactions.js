import React, {Component} from 'react'
import { Tabs , Tab, Table, Button } from 'react-bootstrap'
import makeRequest from '../scripts/common'

export default class mTransactions extends Component {
    constructor(props) {
        super(props);
        this.i = 0
        this.state = {
            Transactions: [],
        };
    }

    async deleteTransaction(event) {
        console.log(event.target.attributes.tid)
        this.getTransactions()
    }

    async getTransactions() {
        const data = await makeRequest("/mess/transactions",{})

        console.log(data)

        if(data['data']['STATUS'] == 'USER_NOT_LOGGED_IN'){
            alert("You're Not Logged In")
            window.location.href="/mess/login"
        }
        try {
            const all = data['data']['transactions']
            .map((t) => 
                <tr>
                    <td>{t.rollno}</td>
                    <td>{t.mess}</td>
                    <td>{t.amount}</td>
                    <td>{t.description}</td>
                    <td>{t.date}</td>
                    <td>{t.time}</td>
                    <td onClick={this.deleteTransaction.bind(this)} tid={t.id}> X </td>
                </tr>)
            this.setState({'Transactions':all.slice(this.i*8,(this.i+1)*8)})
            console.log("Data Set")
        }
        catch(error){
            console.log("GetUserError",error)
        }
    }

    prev(){
        this.i -= 1
        if(this.i < 0)
            this.i = 0
        this.getTransactions()
    }

    next(){
        this.i += 1
        this.getTransactions()
    }

    componentDidMount() {
        this.getTransactions()
    }

    render() {
        return (
            <div id="anchored">
                <Tabs className="tabletab" defaultActiveKey='Transactions'>
                    <Tab className="tabs" eventKey='Transactions' title='Transactions'>
                        <table striped bordered hover id="transactions">
                            <thead>
                                <th>Roll No</th>
                                <th>Mess</th>
                                <th>Amount</th>
                                <th>Description</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Delete</th>
                            </thead>
                            <tbody>
                                {this.state.Transactions}
                            </tbody>
                        </table>
                    </Tab>
                </Tabs>
                <Table striped bordered hover>
                    <thead>
                        <th>
                            <Button onClick={this.prev.bind(this)}>
                                Prev
                            </Button>
                        </th>
                        <th>
                            Page {this.i + 1}
                        </th>
                        <th>
                            <Button onClick={this.next.bind(this)}>
                                Next
                            </Button>
                        </th>
                    </thead>
                </Table>
            </div>
        )
    }
}