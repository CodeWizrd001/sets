import React, {Component} from 'react'
import { Tabs , Tab , Table , Button } from 'react-bootstrap'
import makeRequest from '../scripts/common'

export default class mUsers extends Component {
    constructor(props) {
        super(props);
        this.i = 0
        this.state = {
            Students: [],
        };
    }

    async getUsersData() {
        const data = await makeRequest('/mess/getusers',{})
        console.log(data)
        if(data['data']['STATUS'] == 'USER_NOT_LOGGED_IN'){
            alert("You're Not Logged In")
            window.location.href="/mess/login"
        }
        try {
            const all = data['data']['Students']
            .map((s) => 
                <tr>
                    <td>{s.first_name}</td>
                    <td>{s.last_name}</td>
                    <td>{s.rollno}</td>
                    <td>{s.mess}</td>
                </tr>)
            this.setState({'Students':all.slice(this.i*8,(this.i+1)*8)})
        }
        catch(error){
            console.log("GetUserError",error)
        }
    }

    prev(){
        this.i -= 1
        if(this.i < 0)
            this.i = 0
        this.getUsersData()
    }

    next(){
        this.i += 1
        this.getUsersData()
    }

    componentDidMount(){
        this.getUsersData()
    }
    render() {
        return (
            <div id="anchored">
                <Tabs defaultActiveKey='Student'>
                    <Tab eventKey='Student' title='Student'>
                        <table id="student">
                            <thead>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Roll Number</th>
                                <th>Mess</th>
                            </thead>
                            <tbody>
                                {this.state.Students}
                            </tbody>
                        </table>
                    </Tab>
                </Tabs>
                <Table striped bordered hover>
                    <thead>
                        <th>
                            <Button onClick={this.prev.bind(this)}>
                                Prev
                            </Button>
                        </th>
                        <th>
                            Page {this.i + 1}
                        </th>
                        <th>
                            <Button onClick={this.next.bind(this)}>
                                Next
                            </Button>
                        </th>
                    </thead>
                </Table>
            </div>
        )
    }
}