import React from 'react';
import {logout} from '../scripts/common'
import { Form , Button , Card , Alert } from 'react-bootstrap'

const Mess = (httpObject)=>{
    async function logoutButton(){
        logout(httpObject)
        window.location.href = "/"
    }

    return(
        <div>
            <h1 >Mess</h1>
            <ul>
                <li><a href="/mess/addtransaction">Record Transactions</a></li>
                <li><a href="/mess/transactions">View Transactions</a></li>
                <li><a href="/mess/users">View Users</a></li>
                <li><a onClick={logoutButton}> Logout</a></li>
            </ul>
        </div>
    );
};

export default Mess