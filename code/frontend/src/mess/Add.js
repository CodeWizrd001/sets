import React, {Component , useRef , useEffect , useState} from 'react'
import { Tabs , Tab , Card,Form , Button} from 'react-bootstrap'
import makeRequest from '../scripts/common'

export default class mAdd extends Component {
    constructor(props) {
        super(props);
        this.current = {}
        this.amount = 0.0
        this.description = ""
        this.state = {
            Students: [],
            options: [],
        };
    }

    getUser(roll){
        this.state.Students.forEach((item,index)=>{
            if(item.rollno==roll){
                this.current = item
            }
        })
    }

    async getUserData(){
        const userData = await makeRequest('/mess/getusers',{})
        console.log(userData)
        this.setState({'options':userData.data['Students']
            .map((s)=>
                <option>{s.rollno}</option>)})
        this.setState({'Students':userData.data['Students']})
        this.current = userData.data['Students'][0]
    }

    handleChange(e){
        if(e.target.name=="roll")
            this.getUser(e.target.value)
        else if(e.target.name=='amount'){
            this.amount = e.target.value
        }
        else if(e.target.name=='desc'){
            this.description = e.target.value
            console.log(this.description)
        }
    }

    async handleSubmit(e){
        e.preventDefault()

        const requestBody = {
            'mess' : this.current.mess ,
            'rollno' : this.current.rollno ,
            'amount' : this.amount ,
            'description' : this.description ,
        }

        const data = await makeRequest('/mess/addtransaction',requestBody)

        console.log(data)
        if(data['data']['STATUS'] == 'SUCCESS')
            alert('Recorded')
    }

    componentDidMount() {
        this.getUserData()
    }

    render() {
        return(
            <div>
                <Card>
                    <Card.Body>
                        <Form onSubmit={this.handleSubmit.bind(this)}>
                            <h2 className="text-center mb-4">Add Transaction</h2>
                            <Form.Group>
                                <h5>Roll Number</h5>
                                <Form.Control name="roll" as="select" onChange={this.handleChange.bind(this)} required >
                                    { this.state.options }
                                </Form.Control>
                            </Form.Group>
                            <Form.Group>
                                <h5>Amount</h5>
                                <Form.Control name="amount" type="number" onChange={this.handleChange.bind(this)} required/>
                            </Form.Group>
                            <Form.Group>
                                <h5>Description</h5>
                                <Form.Control name="desc" type="text" onChange={this.handleChange.bind(this)} required/>
                            </Form.Group>
                            <Button className="w-100" type="submit">
                                Confirm
                            </Button>
                        </Form>
                    </Card.Body>
                </Card>
            </div>
        )
    }
}